module gitee.com/jjawesomejj/awesome-rpc

replace gitee.com/jjawesomejj/awesome-rpc => ../awesome-rpc
go 1.16

require (
	github.com/hashicorp/consul/api v1.13.1
	google.golang.org/grpc v1.48.0
	google.golang.org/protobuf v1.28.0
)
