package http

import awesomehttp "gitee.com/jjawesomejj/awesomehttp/system"

type HealthHandler struct {
	awesomehttp.HttpBaseHandler
}

func (handler *HealthHandler) GetModuleName() string {
	return "/"
}

func (handler HealthHandler) ActionHealth() map[string]interface{} {
	return map[string]interface{}{
		"message": "ok",
	}
}
