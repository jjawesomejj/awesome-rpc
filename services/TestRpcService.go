package services

import (
	"context"
	"gitee.com/jjawesomejj/awesome-rpc/servicesRegister/define"
	"math"
)

type TestRpcServices struct {
	define.UnimplementedTestServiceServer
}

func (testRpcServices *TestRpcServices) Add(context context.Context, request *define.TestRequest) (*define.TestResponse, error) {
	return &define.TestResponse{
		Result: int64(math.Pow(float64(request.Number), 2.0)),
	}, nil
}
